#include <stdio.h>
#include <graphics.h>

void drawcircle(int x_centre, int y_centre, int r)
{
    int x = r;
    int y = 0;
    int err = 0;

    while (x >= y)
    {
        putpixel(x_centre + x, y_centre + y, WHITE);
        putpixel(x_centre + y, y_centre + x, WHITE);
        putpixel(x_centre - y, y_centre + x, WHITE);
        putpixel(x_centre - x, y_centre + y, WHITE);
        putpixel(x_centre - x, y_centre - y, WHITE);
        putpixel(x_centre - y, y_centre - x, WHITE);
        putpixel(x_centre + y, y_centre - x, WHITE);
        putpixel(x_centre + x, y_centre - y, WHITE);

        if (err <= 0)
        {
            y += 1;
            err += 2 * y + 1;
        }

        if (err > 0)
        {
            x -= 1;
            err -= 2 * x + 1;
        }
    }
}

int main()
{
    int gdriver = DETECT, gmode, error, x, y, r;
    initgraph(&gdriver, &gmode, "c:\\turboc3\\bgi");

    printf("Enter r of circle: ");
    scanf("%d", &r);

    printf("Enter co-ordinates of center(x and y): ");
    scanf("%d %d", &x, &y);
    drawcircle(x, y, r);
    getch();
    return 0;
}