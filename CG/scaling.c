#include<graphics.h>
#include<conio.h>
#include <stdio.h>
#include <math.h>
int x1 = 300;
int y1 = 300;
float r1 = 50;
int x2 = 300;
int y2 = 300;
int r2 = 50;
void transform(int tx, int ty) {
	x1 += tx;
	y1 += ty;
	circle(x1, y1, r1) ;
}

void reset() {
	x1 = x2;
	y1 = y2;
	r1 = r2;
	clrscr();
	circle(x1, y1, r1);
}
void scale(float rx){
	r1 *= rx;
	circle(x1, y1, r1);
}

void rotate(){
    int angle;
    printf("Enter the value for angle: ");
    scanf("%d", &angle);
    angle = angle * (3.14/ 180);
    x1 = x1 * sin(angle) - y1 * cos(angle);
    y1 = x1 * sin(angle) + y1 * cos(angle); 
    circle(x1, y1, r2);

}

int main()
{
   int gd = DETECT, gm;
   int c = 1;
   int tx, ty;
   float rx;
   initgraph(&gd, &gm, "C:\\TURBOC3\\BGI");

   circle(x1, y1, r1);
   while (c > 0) {
	printf("1.Transform\n2.Scale\n3.Reset\n4.Rotate\n5.Exit\n");
	scanf("%d", &c);
	switch (c)
	{
	case 1:
	    printf("Enter the value for Tx and Ty: ");
	    scanf("%d %d", &tx, &ty);
	    transform(tx, ty);
	    break;
	case 2:
	    printf("Enter the value for Rx: ");
	    scanf("%f", &rx);
	    scale(rx);
	    break;
	case 3:
	    reset();
	    break;

    case 4:
	rotate();
	break;
	case 5:
	    c = -1;
	}

   }
   getch();
   closegraph();
   return 0;
}

