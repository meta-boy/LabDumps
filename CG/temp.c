#include <stdio.h>
#include <conio.h>
#include <graphics.h>
#include <dos.h>
void ff(int x, int y, int f, int b)
{
    int c;
    c = getpixel(x, y);
    if ((c != b) && (c != f))
    {
        setcolor(f);
        putpixel(x, y, f);
        delay(10);
        ff(x + 1, y, f, b);
        ff(x, y + 1, f, b);
        ff(x + 1, y + 1, f, b);
        ff(x - 1, y - 1, f, b);
        ff(x - 1, y, f, b);
        ff(x, y - 1, f, b);
        ff(x - 1, y + 1, f, b);
        ff(x + 1, y - 1, f, b);
    }
}
void main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "c:\\turboc3\\bgi");
    rectangle(50, 50, 100, 100);
    ff(55, 55, 4, 15);
    getch();
    closegraph();
}
