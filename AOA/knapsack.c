#include<stdio.h>
#include<time.h>
#include<conio.h>
void knapsack(float capacity, int n, float weight[], float profit[])
{
	float x[20], totalprofit,y;
	int i,j;
	y=capacity;
	totalprofit=0;
	for(i=0;i < n;i++)
		x[i]=0.0;
	for(i=0;i < n;i++)
	{
		if(weight[i] > y)
			break;
		else
		{
			x[i]=1.0;
			totalprofit=totalprofit+profit[i];
			y=y-weight[i];
		}
	}
	if(i < n)	
		x[i]=y/weight[i];
	totalprofit=totalprofit+(x[i]*profit[i]);
	printf("The selected elements are:-\n ");
	for(i=0;i < n;i++)
		if(x[i]==1.0)
			printf("\nProfit is %.2f with weight %.2f ", profit[i], weight[i]);
		else if(x[i] > 0.0)
			printf("\n%.2f part of Profit %.2f with weight %.2f", x[i], profit[i], weight[i]);
	printf("\nTotal profit for %d objects with capacity %.2f = %.2f\n\n", n, capacity,totalprofit);
}			
void main()
{
	float weight[20],profit[20],ratio[20], t1,t2,t3;
	int n;
	time_t start,stop;
	float capacity;
	int i,j;
	printf("Enter number of objects:  ");
	scanf("%d", &n);
	printf("\nEnter the capacity of knapsack: ");
	scanf("%f", &capacity);
	for(i=0;i < n;i++)
	{
		printf("\nEnter %d(th)  profit: ", (i+1));
		scanf("%f", &profit[i]);
		printf("Enter %d(th)  weight: ", (i+1));
		scanf("%f", &weight[i]);
		ratio[i]=profit[i]/weight[i];
	}
	start=time(NULL);
	for(i=0;i < n;i++)
		for(j=0;j < n;j++)
		{
			if(ratio[i] > ratio[j])
			{
				t1=ratio[i];
				ratio[i]=ratio[j];
				ratio[j]=t1;
				t2=weight[i];
				weight[i]=weight[j];
				weight[j]=t2;
				t3=profit[i];
				profit[i]=profit[j];
				profit[j]=t3;
			}
		}
	knapsack(capacity,n,weight,profit);
}


/* OUTPUT:


D:\Work\Workspace\Miscellaneous\LabDumps\AOA>gcc knapsack.c -o knapsack.out

D:\Work\Workspace\Miscellaneous\LabDumps\AOA>knapsack.out
Enter number of objects:  4

Enter the capacity of knapsack: 10

Enter 1(th)  profit: 10
Enter 1(th)  weight: 5

Enter 2(th)  profit: 40
Enter 2(th)  weight: 4

Enter 3(th)  profit: 30
Enter 3(th)  weight: 6

Enter 4(th)  profit: 50
Enter 4(th)  weight: 3
The selected elements are:-

Profit is 50.00 with weight 3.00
Profit is 40.00 with weight 4.00
0.50 part of Profit 30.00 with weight 6.00
Total profit for 4 objects with capacity 10.00 = 105.00

*/