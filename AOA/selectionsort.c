#include <stdio.h>

int counter = 0;

void swap(int *xp, int *yp){
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void selectionSort(int array[], int n){
    int i, j, minIndex;

    for (i = 0; i < n - 1; i++){
        minIndex = i;
        for (j = i + 1; j < n; j++)
            if (array[j] < array[minIndex])
                minIndex = j;
                
        swap(&array[minIndex], &array[i]);
        counter++;

    }
}

void displayArray(int array[], int size){
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main(){
    int array[100], n, i;
    printf("Enter size of the array ( MAX 100 ): ");
    scanf("%d", &n);
    for (i = 0; i < n; i++){
        printf("Enter the element at position %d: ", i);
        scanf("%d", &array[i]);
    }
    selectionSort(array, n);
    printf("Sorted Array: \n");
    displayArray(array, n);
    printf("Compared %d times\n", counter);
    return 0;
}