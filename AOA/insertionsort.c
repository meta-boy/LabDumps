#include <stdio.h>
int counter = 0;

void insertionSort(int arr[], int n){
    int i, key, j;
    for (i = 1; i < n; i++){
        key = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > key){
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
        counter++;
    }
}

void displayArray(int array[], int size){
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main(){
    int array[100], i, j, min, temp, n;
    printf("Enter size of the array ( MAX 100 ): ");
    scanf("%d", &n);
    for (i = 0; i < n; i++){
        printf("Enter the element at position %d: ", i);
        scanf("%d", &array[i]);
    }
    printf("Entered Array is: ");
    displayArray(array, n);
    insertionSort(array, n);
    printf("Sorted array is: ");
    displayArray(array, n);
    printf("Counter counted %d times", counter);

    return 0;
}