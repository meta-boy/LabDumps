#include <stdio.h>

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void selectionSort(int array[], int n)
{
    int i, j, minIndex;

    for (i = 0; i < n - 1; i++)
    {
        minIndex = i;
        for (j = i + 1; j < n; j++)
            if (array[j] < array[minIndex])
                minIndex = j;

        swap(&array[minIndex], &array[i]);
    }
}

void displayArray(int array[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int binarySearch(int arr[], int l, int r, int x)
{
    if (r >= l)
    {
        int mid = l + (r - l) / 2;

        if (arr[mid] == x)
            return mid;

        if (arr[mid] > x)
            return binarySearch(arr, l, mid - 1, x);

        return binarySearch(arr, mid + 1, r, x);
    }

    return -1;
}

int main(void)
{
    int arr[100], n, i, x;
    printf("Enter size of the array ( MAX 100 ): ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("Enter the element at position %d: ", i);
        scanf("%d", &arr[i]);
    }
    printf("Enter element to search: ");
    scanf("%d", &x);
    selectionSort(arr, n);
    printf("Sorted array:");
    displayArray(arr, n);

    int result = binarySearch(arr, 0, n - 1, x);
    (result == -1) ? printf("Element not found")
                   : printf("Element found at index %d",
                            result);
    return 0;
}
