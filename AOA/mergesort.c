#include <stdio.h>

// #define max 10

int a[100] = {10, 14, 19, 26, 27, 31, 33, 35, 42, 44, 0};
int b[100];

void combine(int low, int mid, int high)
{
    int l1, l2, i;

    for (l1 = low, l2 = mid + 1, i = low; l1 <= mid && l2 <= high; i++)
    {
        if (a[l1] <= a[l2])
            b[i] = a[l1++];
        else
            b[i] = a[l2++];
    }

    while (l1 <= mid)
        b[i++] = a[l1++];

    while (l2 <= high)
        b[i++] = a[l2++];

    for (i = low; i <= high; i++)
        a[i] = b[i];
}

void displayArray(int array[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

void mergeSort(int low, int high)
{
    int mid;

    if (low < high)
    {
        mid = (low + high) / 2;
        mergeSort(low, mid);
        mergeSort(mid + 1, high);
        combine(low, mid, high);
    }
    else
    {
        return;
    }
}

int main()
{
    int i, n;
    printf("Enter size of the array ( MAX 100 ): ");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("Enter the element at position %d: ", i);
        scanf("%d", &a[i]);
    }
    
    printf("List before Sorting\n");
    displayArray(a, n);

    mergeSort(0, n);

    printf("\nList after Sorting\n");
    displayArray(a, n);
}