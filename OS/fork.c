#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main()
{
    int n;
    pid_t pid, mpid, mppid;
    printf("Enter number of child processes you want to make: ");
    scanf("%d", &n);

    for ( int i = 0; i < n; i++){
        pid = getpid();
        printf("Before fork: PID = %d\n", pid);
        pid = fork();
        if (pid < 0)
        {
            perror("fork() failure\n");
            return 1;
        }
        if (pid == 0)
        {
            printf("--PARENT PROCESS--\n");
            mpid = getpid();
            mppid = getppid();
            printf("PID =  %d\nPPID = %d\n", mpid, mppid);
        }
        else
        { 
            sleep(2);
            printf("--CHILD PROCESS--\n");
            mpid = getpid();
            mppid = getppid();
            printf("PID = %d\nPPID = %d\n", mppid, mpid);
            printf("CHILD PID = %d\n", pid);
            printf("\n\n");
    }
    }
    return 0;
}

/*
OUTPUT: 

Enter number of child processes you want to make: 2
Before fork: PID = 931
--PARENT PROCESS--
PID =  932
PPID = 931
Before fork: PID = 932
--PARENT PROCESS--
PID =  933
PPID = 932
--CHILD PROCESS--
PID = 393
PPID = 931
CHILD PID = 932


Before fork: PID = 931
--CHILD PROCESS--
PID = 931
PPID = 932
CHILD PID = 933


--PARENT PROCESS--
PID =  934
PPID = 931
--CHILD PROCESS--
PID = 393
PPID = 931
CHILD PID = 934
 */
