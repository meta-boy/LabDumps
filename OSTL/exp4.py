studentsList = []
c = 1

menu = {
    "1": "Enter info for a student",
    "2": "Get student info",
    "3": "Sort students by name",
    "4": "Display list",
    "5": "Exit\n\n\n"
}


def displayInfo(thing):
    for i in thing:

        print(
            f"""
roll number = {i[0]}
name = {i[1]}
marks in CG, OSTL and AOA:
{",".join(list(i)[2:])}
"""
        )


def makeTuple():
    x = tuple(
        input("Enter roll number, name and marks for CG, OSTL and AOA comma seperated (', '): ").split(", "))
    studentsList.append(x)


while c != 0:
    for i in menu:
        print(f"{i}. {menu[i]}")
    userin = input("Enter your choice:\n>")

    if userin in menu:
        if userin == "1":
            makeTuple()
        elif userin == "2":
            name = input("Enter student name: ")
            for i in studentsList:
                if name in i:
                    displayInfo([list(i)])
        elif userin == "3":
            try:
                studentsList = sorted(
                    tuple(studentsList), key=lambda student: student[1])
                displayInfo(studentsList)
            except Exception as e:
                print("You haven't entered a value to get the palindrome")
        elif userin == "4":
            displayInfo(studentsList)

        elif userin == "5":
            c = 0
