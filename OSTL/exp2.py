c = 1
menu = {
    "1": "Enter a number:",
    "2": "Check Palindrome",
    "3": "Check Factorial",
    "4": "Exit\n\n\n"
}


def isPalindrome(thing: str):
    if thing == thing[::-1]:
        return True

    return False


def factorial(thing: int):
    answer = 1
    for i in range(1, thing + 1):
        answer *= i

    return answer


while c != 0:
    for i in menu:
        print(f"{i}. {menu[i]}")
    userin = input("Enter your choice:\n>")

    if userin in menu:
        if userin == "1":
            x = input(menu[userin])
        elif userin == "2":
            try:
                if isPalindrome(x):
                    print("It is a palindrome")
                else:
                    print("It is not a palindrome")
            except Exception as e:
                print("You haven't entered a value to get the palindrome")
        elif userin == "3":
            try:
                print(factorial(int(x)))
            except Exception as e:
                print("You haven't entered a value to get the palindrome")
        elif userin == "4":
            c = 0
