import os



def readFile(file: str = "exp7test.txt"):
    file = open(file, "r")
    x = file.read()
    print(x)
    file.close()
    return x

def writeToNewFile():
    file = open("exp7testCopy.txt", "w")
    file.write(readFile())
    file.close()
    readFile("exp7testCopy.txt")

def pyLs():
    for root, dirs, files in os.walk("."):
        print("Files in current directory: \n")
        for filename in files:
            print(filename)

def appendToFile():
    x = input("Enter the data to append to the file: ")
    file = open("exp7test.txt", "a")
    file.write(x)
    file.close()
    readFile()

def getDemo():
    file = readFile()
    num_lines = 0
    for line in file.split("\n"):
        num_lines += 1
    num_words = len(file.split(" ")) + 1
    num_chars = len(file)
    print("Number of lines: ", num_lines)
    print("Number of words: ", num_words)
    print("Number of characters: ", num_chars)

writeToNewFile()
appendToFile()
getDemo()
pyLs()


# C:\Users\Source\Documents\Workspace\Miscellaneous\LabDumps\OSTL>python3.6 exp7.py
# Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small 
# and large-scale projects.[28]

# Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a "batteries included" language due to its comprehensive standard library.[29]

# Python was conceived in the late 1980s as a successor to the ABC language. Python 2.0, released in 2000, introduced features like list comprehensions and a garbage collection system capable of collecting reference cycles. Python 3.0, released in 2008, was a major revision of the language that is not completely backward-compatible, and much Python 2 code does not run unmodified on Python 3.

# The Python 2 language, i.e. Python 2.7.x, was officially discontinued on 1 January 2020 (first planned for 2015) after which security patches and other improvements will not 
# be released for it.[30][31] With Python 2's end-of-life, only Python 3.5.x[32] and later are supported.

# Python interpreters are available for many operating systems. A global community of programmers develops and maintains CPython, an open source[33] reference implementation. A non-profit organization, the Python Software Foundation, manages and directs resources for Python and CPython development.
# Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small 
# and large-scale projects.[28]

# Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a "batteries included" language due to its comprehensive standard library.[29]

# Python was conceived in the late 1980s as a successor to the ABC language. Python 2.0, released in 2000, introduced features like list comprehensions and a garbage collection system capable of collecting reference cycles. Python 3.0, released in 2008, was a major revision of the language that is not completely backward-compatible, and much Python 2 code does not run unmodified on Python 3.

# The Python 2 language, i.e. Python 2.7.x, was officially discontinued on 1 January 2020 (first planned for 2015) after which security patches and other improvements will not 
# be released for it.[30][31] With Python 2's end-of-life, only Python 3.5.x[32] and later are supported.

# Python interpreters are available for many operating systems. A global community of programmers develops and maintains CPython, an open source[33] reference implementation. A non-profit organization, the Python Software Foundation, manages and directs resources for Python and CPython development.
# Enter the data to append to the file: \n\n\n\n\n\n\nOk Boomer
# Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small 
# and large-scale projects.[28]

# Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a "batteries included" language due to its comprehensive standard library.[29]

# Python was conceived in the late 1980s as a successor to the ABC language. Python 2.0, released in 2000, introduced features like list comprehensions and a garbage collection system capable of collecting reference cycles. Python 3.0, released in 2008, was a major revision of the language that is not completely backward-compatible, and much Python 2 code does not run unmodified on Python 3.

# The Python 2 language, i.e. Python 2.7.x, was officially discontinued on 1 January 2020 (first planned for 2015) after which security patches and other improvements will not 
# be released for it.[30][31] With Python 2's end-of-life, only Python 3.5.x[32] and later are supported.

# Python interpreters are available for many operating systems. A global community of programmers develops and maintains CPython, an open source[33] reference implementation. A non-profit organization, the Python Software Foundation, manages and directs resources for Python and CPython development.\n\n\n\n\n\n\nOk Boomer
# Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small 
# and large-scale projects.[28]

# Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a "batteries included" language due to its comprehensive standard library.[29]

# Python was conceived in the late 1980s as a successor to the ABC language. Python 2.0, released in 2000, introduced features like list comprehensions and a garbage collection system capable of collecting reference cycles. Python 3.0, released in 2008, was a major revision of the language that is not completely backward-compatible, and much Python 2 code does not run unmodified on Python 3.

# The Python 2 language, i.e. Python 2.7.x, was officially discontinued on 1 January 2020 (first planned for 2015) after which security patches and other improvements will not 
# be released for it.[30][31] With Python 2's end-of-life, only Python 3.5.x[32] and later are supported.

# Python interpreters are available for many operating systems. A global community of programmers develops and maintains CPython, an open source[33] reference implementation. A non-profit organization, the Python Software Foundation, manages and directs resources for Python and CPython development.\n\n\n\n\n\n\nOk Boomer
# Number of lines:  9
# Number of words:  223
# Number of characters:  1643
# Files in current directory:

# exp1.py
# exp2.py
# exp3.py
# exp4.py
# exp6.py
# exp7.py
# exp7test.txt
# exp7testCopy.txt
# OSTL_Lab_Manual_17_18_Even_final (1).pdf
# Files in current directory:

# print-ostl-26-1-2020.docx
# print-ostl-3-2-2020.docx
