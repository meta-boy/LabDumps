odd = []
even = []
c = 1
numberList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
              22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]

print("Number list: \n", ", ".join([str(x) for x in numberList]))


def isOdd(number: int):
    if number % 2 == 0:
        return False

    return True


for num in numberList:
    if isOdd(num):
        odd.append(num)
    else:
        even.append(num)

print("Odd number list: \n", ", ".join([str(x) for x in odd]))
print("Even number list: \n", ", ".join([str(x) for x in even]))


mergedList = odd + even
print("Merged list: \n", mergedList)
sortedList = sorted(mergedList)
print("Sorted list: \n", sortedList)
print(f"Max: {max(sortedList)}, Min: {min(sortedList)}")
x = int(input("Enter some number: \n>"))
sortedList[0] = x
print(", ".join([str(x) for x in sortedList]))
sortedList.remove(sortedList[int(len(sortedList)/2)])
print(", ".join([str(x) for x in sortedList]))

nNames = ["Anurag", "Patil", "18CE8004", "Python", "HEllo world"]
print("Adding the following names to the list: ",
      ",".join([str(x) for x in nNames]))
sortedList += nNames
print("The List now is: \n", sortedList)

if "Python" in sortedList:
    print("The word \"Python\" is present in the list")
