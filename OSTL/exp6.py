class Experiment6:
    def __init__(self):
        self.dictionary = {
            
        }
        self.c = 0

    def enter(self, key, value):
        self.dictionary[key] = value

    def display(self, li = None):
        if not li:
            li = self.dictionary
        for key, value in li.items():
            print(key,",", value)

    def delete(self, key):
        del self.dictionary[key]

    def run(self):

        while self.c == 0:
            print(
                """
                A. Create a New Entry
                B. Delete an Entry
                C. Modify an Entry
                D. Find an Entry    
                E. Zip 2 lists
                F. Exit
                """
            )
            x = input("Enter your choice: ").capitalize()
            if x == "A":
                self.enter(input("Enter the key: "),
                           input("Enter the value: "))
                self.display()
            elif x == "B":
                self.delete(
                    input("Enter key of the entry you wish to delete: "))
                print("DELETED")
                self.display()

            elif x == "C":
                key = input("Enter the key you wish to modify: ")
                try:

                    a = self.dictionary[key]
                    a = input("Enter the new Value")
                    self.dictionary[key] = a
                except expression as identifier:
                    print("The key you entered does not exist in the dictionary")
                self.display()

            elif x == "D":
                key = input("Enter the key you wish to find: ")
                try:
                    a = self.dictionary[key]
                    print(key, ", ", a)
                except expression as identifier:
                    print("The key you entered does not exist in the dictionary")

            elif x == "E":
                list1 = input("Enter 1st list, space seperated: ").split(" ")
                list2 = input("Enter 2nd list, space seperated: ").split(" ")
                mapped = zip(list1, list2)
                self.display(dict(mapped))
            elif x == "F":
                self.c = 5
                print("Final dictionary is: \n")
                self.display()
                print("Exiting...")
            else:
                print("Invalid")


exp6 = Experiment6()
exp6.run()
